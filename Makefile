REGISTRY=registry.gitlab.com
USERNAME=supernami
VERSION=1.0

### NG base
ng-base: ng-build ng-tag
ng-base-push: ng-build ng-tag ng-push

ng-build:
	docker build -t ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/base:${VERSION} -f ng.df .

ng-tag:
	docker tag ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/base:${VERSION} ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/base:latest

ng-push:
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/base-${VERSION} && \
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/ng/base-latest

### NPCG base
npcg-base: npcg-build npcg-tag
npcg-base-push: npcg-build npcg-tag npcg-push

npcg-build:
	docker build -t ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/base:${VERSION} -f npcg.df .

npcg-tag:
	docker tag ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/base:${VERSION} ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/base:latest

npcg-push:
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/base:${VERSION} && \
	docker push ${REGISTRY}/${USERNAME}/docker-nginx-images/npcg/base:latest
